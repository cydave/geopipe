module gitlab.com/lu-ka/geopipe

go 1.18

require (
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d
	github.com/oschwald/maxminddb-golang v1.9.0
	github.com/projectdiscovery/retryabledns v1.0.14
	golang.org/x/exp v0.0.0-20220722155223-a9213eeb770e
)

require (
	github.com/karrick/godirwalk v1.16.1 // indirect
	github.com/miekg/dns v1.1.50 // indirect
	github.com/projectdiscovery/blackrock v0.0.0-20210415162320-b38689ae3a2e // indirect
	github.com/projectdiscovery/fileutil v0.0.0-20210926202739-6050d0acf73c // indirect
	github.com/projectdiscovery/iputil v0.0.0-20210804143329-3a30fcde43f3 // indirect
	github.com/projectdiscovery/mapcidr v0.0.7 // indirect
	github.com/projectdiscovery/retryablehttp-go v1.0.2 // indirect
	github.com/projectdiscovery/sliceutil v0.0.0-20220225084130-8392ac12fa6d // indirect
	github.com/projectdiscovery/stringsutil v0.0.0-20210823090203-2f5f137e8e1d // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/net v0.0.0-20220728211354-c7608f3a8462 // indirect
	golang.org/x/sys v0.0.0-20220731174439-a90be440212d // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.12 // indirect
)
